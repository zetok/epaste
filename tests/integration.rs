
// TODO change filename to something better(?)

// TODO get `ENV_KEYFILE` and `ENV_PASSWD` values from epaste source instead
// of hardcoding them?

// TODO test also non-utf8 passwords
//  - currently only correctly encoded UTF-8 passwords are tested
//  - passwords supplied via keyfile can contain any bytes
//  - passwords supplied via env and as an arg can contain any bytes
//    except for null (\0)

// TODO test encrypted data with <4 trailing newlines/whitespaces

extern crate assert_cmd;

extern crate failure;
use failure::Error;

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate quickcheck;
use quickcheck::TestResult;

extern crate rand;
use rand::Rng;

extern crate tempfile;
use tempfile::NamedTempFile;

use std::ffi::OsStr;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;
use std::process::Command;
use std::process::*;
use std::sync::Arc;
use std::thread;


/// Bytes that are allowed to be printed to stdout after encryption.
const ALLOWED_BYTES: &[u8] = &[
    b'a', b'b', b'c', b'd', b'e', b'f', b'g', b'h', b'i', b'j', b'k', b'l',
    b'm', b'n', b'o', b'p', b'q', b'r', b's', b't', b'u', b'v', b'w', b'x',
    b'y', b'z',
    b'A', b'B', b'C', b'D', b'E', b'F', b'G', b'H', b'I', b'J', b'K', b'L',
    b'M', b'N', b'O', b'P', b'Q', b'R', b'S', b'T', b'U', b'V', b'W', b'X',
    b'Y', b'Z',
    b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9',
    b'/', b'+', b'=',
    b'.',

    // b'\n', b'\r', // old v1 format, v3 no longer uses base64 with newlines
];


lazy_static! {
    static ref BIN_PATH: PathBuf = assert_cmd::cargo::main_binary_path().unwrap();
}

#[derive(Debug)]
struct TmpFile {
    file: NamedTempFile,
    path: PathBuf,
}

impl TmpFile {
    // Create a new, empty `TmpFile`.
    fn new() -> Self {
        let file = NamedTempFile::new().unwrap();
        let path = file.path().to_path_buf();
        TmpFile { file: file, path: path }
    }

    // Create `TmpFile` and write supplied bytes.
    fn from_bytes<B: AsRef<[u8]>>(bytes: B) -> Self {
        let mut tf = TmpFile::new();
        tf.as_file_mut().write_all(bytes.as_ref()).unwrap();
        tf
    }

    fn as_file_mut(&mut self) -> &mut File {
        self.file.as_file_mut()
    }

    fn path(&self) -> &PathBuf {
        &self.path
    }
}


fn spawn_process<B, I, K, V, A, S>(args: A, input: B, env: Option<I>)
                                   -> Result<Output, Error>
where B: AsRef<[u8]>,
      I: IntoIterator<Item = (K, V)>,
      K: AsRef<OsStr>,
      V: AsRef<OsStr>,
      A: AsRef<[S]>,
      S: AsRef<OsStr>
{
    let mut proc = match env {
        Some(e) => {
            Command::new(&*BIN_PATH)
                .args(args.as_ref())
                .envs(e)
                .stdin(Stdio::piped())
                .stdout(Stdio::piped())
                .stderr(Stdio::piped())
                .spawn()?
        },
        None => {
            Command::new(&*BIN_PATH)
                .args(args.as_ref())
                .stdin(Stdio::piped())
                .stdout(Stdio::piped())
                .stderr(Stdio::piped())
                .spawn()?
        },
    };

    // ↓ magical way of getting stdi/o to work and to get output
    {
        let mut stdin = proc.stdin.take().unwrap();
        let input = input.as_ref().to_owned();
        thread::spawn(move || {
            stdin.write_all(&input).unwrap();
        });
    }

    Ok(proc.wait_with_output()?)
}

/**
Function to check whether all bytes match any of the bytes that are supposed to
be used by `epaste`.
*/
fn verify_output_bytes<B: AsRef<[u8]>>(bytes: B) -> bool {
    bytes.as_ref().iter().all(|b| ALLOWED_BYTES.contains(b))
}


/// Panics if decrypting fails or output doesn't match expectations.
fn verify_decrypt(passwd: &str, input: &[u8], output: &[u8]) {
    fn check_with_args<A, S, K, I, V>(args: A,
                                      env: Option<I>,
                                      input: &[u8],
                                      output: &[u8])
    where A: AsRef<[S]>,
          S: AsRef<OsStr>,
          I: IntoIterator<Item = (K, V)>,
          K: AsRef<OsStr>,
          V: AsRef<OsStr>
    {
        let plain = spawn_process(args, input, env).unwrap();
        assert!(plain.status.success());
        assert_eq!(plain.stdout, output);
        assert!(plain.stderr.is_empty());
        assert!(input != &plain.stdout[..]);
    }

    macro_rules! check_with {
        ($($args:expr, $env:expr),+) => ($(
            check_with_args($args, $env, input, output);
        )+)
    }

    // write password to file and get the path
    let keyfile = TmpFile::from_bytes(passwd.as_bytes());

    check_with!(
        // password supplied via CLI
        ["-d", passwd], None as Option<Vec<(&str, &str)>>,
        //["-d", passwd], n,
        // password via env
        ["-d"], Some(vec![("EPC_PASSWD", passwd)]),
        // keyfile
        ["-d"], Some(vec![("EPC_KEYFILE", keyfile.path().as_os_str())])
    );
}


/// Returns `Output` of encryption run.
fn encrypt_decrypt(passwd: String, data: Vec<u8>) {
    fn check_with<K, V>(passwd: &String,
                        data: &Arc<Vec<u8>>,
                        env: Option<Vec<(K, V)>>)
                        -> std::thread::JoinHandle<std::process::Output>
    where K: AsRef<OsStr> + Send + 'static,
          V: AsRef<OsStr> + Send + 'static
    {
        let args = match env {
            Some(_) => vec![],
            None => vec![passwd.clone()],
        };
        let data = Arc::clone(data);
        let passwd = passwd.clone();

        thread::spawn(move || {
            let output = spawn_process(args, &*data, env).unwrap();
            assert!(output.status.success());
            assert!(!output.stdout.is_empty());
            assert!(output.stderr.is_empty());
            assert!(verify_output_bytes(&output.stdout));

            // decrypting encrypted data also works
            verify_decrypt(&passwd, &output.stdout, (*data).as_ref());

            output
        })
    }


    let data = Arc::new(data);

    // first check if it works with password via CLI arg
    let p1 = check_with(&passwd, &data, None as Option<Vec<(&str, &str)>>);
    let p2 = check_with(&passwd, &data, None as Option<Vec<(&str, &str)>>);
    // and then check with password via env
    let env = Some(vec![("EPC_PASSWD", passwd.clone())]);
    let e1 = check_with(&passwd, &data, env.clone());
    let e2 = check_with(&passwd, &data, env.clone());
    // and with a keyfile
    let keyfile = TmpFile::from_bytes(passwd.as_bytes());
    let env = Some(vec![("EPC_KEYFILE", keyfile.path().as_os_str().to_owned())]);
    let k1 = check_with(&passwd, &data, env.clone());
    let k2 = check_with(&passwd, &data, env.clone());

    fn assert_output_content_differs(v: &[std::process::Output]) {
        if v.len() > 1 {
            assert_eq!(v[0].stdout.len(), v[1].stdout.len());
            assert!(v[0].stdout != v[1].stdout);
            assert_output_content_differs(&v[1..]);
        }
    }

    macro_rules! join_and_neq_output {
        ($($t:ident),+) => (
            // wait for all threads to finish
            $(let $t = $t.join().unwrap();)+
                assert_output_content_differs(&[$($t),+]);
        )
    }
    join_and_neq_output!(p1, p2, e1, e2, k1, k2);
}


quickcheck! {
    fn encrypt_decrypt_random(passwd: String, data: Vec<u8>) -> TestResult {
        if passwd.contains("\0") || data.is_empty() {
            return TestResult::discard()
        }
        encrypt_decrypt(passwd, data);
        TestResult::passed()
    }

}

#[test]
fn encrypt_decrypt_random_large() {
    let passwd = "password".to_owned();

    // for some reason it fails to finish with some bigger sizes
    let mut v = vec![0; 1024 * 1024 * 64];
    rand::thread_rng().fill(&mut v[..]);
    encrypt_decrypt(passwd, v);
}


#[test]
fn decrypt_old_v1() {
    // TODO add more inputs?

    /*
    Data format changed in v3.x.

    Change in data format removed newlines and a presence of thereof will result
    in an error. Aside from that, old data format with newlines removed is the
    same as data format from v3.x.
    */
    let ciphertext: Vec<u8> = include_bytes!("encrypted_asdf\\n_pass.epc")
        .iter()
        .filter(|b| **b != b'\n' && **b != b'\r')
        .map(|b| *b)
        .collect();

    let plain = b"asdf\n";
    let passwd = "pass";
    verify_decrypt(passwd, &ciphertext, plain);
}
