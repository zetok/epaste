# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic
Versioning](https://semver.org/spec/v2.0.0.html) since the epaste version 2.0.0.


## Unreleased

### Added

- Environment variables `EPC_KEYFILE` and `EPC_PASSWD` are now checked, and if
  available they're used instead of the password supplied as an argument. If
  more than one variable is defined, or a variable and a password are supplied
  at the same time, Epaste will error out.

### Changed

- Data format changed, newlines (`\n`, `\r`) are no longer used. This allowed to
  speed up base64 decoding by about 500% (on my box from ~45 MB/s to ~245 MB/s)
  and encoding by about 6% (~305 MB/s → ~325 MB/s). **After the change old
  format is no longer compatible.** To decrypt old data newlines need to be
  removed from it, e.g. by using tool `tr` from `coreutils`: `tr -d '\n\r'
  <old_file.epc | epaste -d`. In case where newlines are needed in the new data
  format, tool `fold` from `coreutils` can be used, e.g.: `epaste <plaintext |
  fold > encrypted.epc`.
- Further performance improvements by adjusting buffer sizes.
- Improved error message when decryption fails, to clearly state that either key
  or data is invalid.


## v2.0.0 - 2018-09-22

### Added

- Automated project testing. Already helped catch one bug, fixed in this
  release. In the future it will also help catch regressions.

### Changed

- Error and help messages are now written to stderr instead of stdout.

### Deprecated

- Current `epaste-gui` that uses `kdialog` to work is now deprecated.

### Removed

- `-dr` switch has been removed, after being previously deprecated.

### Fixed

- Fixed problem with some data not being written to stdout correctly when it
  begun with one or more null bytes (`\0`). All data that previously did decrypt
  correctly but was not fully written to stdout now will be fully written out.
  See commit `35a2fb6e420b3378941edd137aeb3090c9c3e574` for more info.


<a name=""></a>
## v1.1.0 Year Later (2017-04-11)

Main improvement is reworked handling of data stream – encrypting/decrypting
of large data streams no longer requires a lot of RAM.

1.1.0 can decrypt data encrypted with older versions.

Older versions cannot decrypt files created with newer version.

Note that decrypting legacy (pre 1.1.0) files that are big still requires a lot
of RAM.

#### Features

* Support encrypting/decrypting data streams up to 10EB big.

#### Fixes

* Corrected formatting of error message about wrong arguments.
* Kdialog GUI shows an error message when decrypting fails.

#### Breaking changes

* `-d` switch no longer appends a newline character (`\n`) after the decrypted
  data, behaviour is now the same as `-dr` switch.
* `-dr` switch is now deprecated and is going to be removed in `v2.0.0`. Use
  `-d` instead.


<a name=""></a>
## v1.0.2  (2016-04-18)

#### Features

* **CLI:**  print help message if CLI argument is `-h` or `--help` ([96f8a5be](96f8a5be))



<a name=""></a>
## v1.0.0 The First (2016-04-17)

Release 1.0.0.

#### Features

*   working CLI interface
