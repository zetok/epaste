/*
    Copyright © 2016-2020 Zetok Zalbavar <zexavexxe@gmail.com>

    This file is part of Epaste.

    Epaste is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Epaste is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epaste.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
Currently just read stdin to the end, encrypt that with a `Key` derived
from the supplied password, then concatenate `Nonce`, `Salt` and the payload,
base64 data and print it to stdout.

In a case where `-h`, `--help` or too many/little arguments are passed print
help message & exit.

# Format of serialized data

Data in serialised format is split into "segments" delimited by `.` (dot).

First segment contains:

No. bytes | Data
----------|-----
`NONCEBYTES`    | Bytes of a Nonce
`SALTBYTES`     | Bytes of a Salt
`[MACBYTES, …]` | Bytes of encrypted payload, up to `10MiB + MACBYTES`

Nonce from the first segment is incremented for each of the following segments.
Following segments contain only encrypted data and `MACBYTES`, i.e.:

No. bytes | Data
----------|-----
`[MACBYTES, …]` | Bytes of encrypted payload, up to `10MiB + MACBYTES`
*/


#![warn(missing_docs)]


extern crate failure;

use failure::Error;

#[macro_use]
extern crate failure_derive;
#[macro_use]
extern crate lazy_static;
extern crate sodiumoxide;

#[cfg(test)]
#[macro_use]
extern crate quickcheck;
#[cfg(test)]
extern crate rand;

mod base64;

use base64::{base64_decode, base64_encode};

mod crypto;

use crypto::{
    Header,
    HEADER_BYTES,
    MACBYTES,
    Key,
    decrypt,
    encrypt,
    get_key,
};

use std::env;
use std::fs::File;
use std::io::{
    self,
    BufRead,
    Read,
    Write,
    Stdout
};
use std::os::unix::ffi::OsStrExt;
use std::process;



// TODO use log crate + env_logger with printing to stderr

// TODO improve read buffering and passing data, so that the encryption part
// doesn't need to waste time waiting for data to be read from stdin


/// Help message.
// TODO use `clap` or some other crate for it
fn help() {
    eprintln!("Licensed under GNU GPLv3+ © 2016-2020 Zetok Zalbavar.

`epaste` usage:

To encrypt:

    epaste 'password' < file

To decrypt:

    epaste -d 'password' < encrypted.epc > plaintext_file


When decrypting non-plaintext data, make sure that it's not written to \
terminal output.");
}


/// Input section failure.
#[derive(Debug, Fail)]
#[fail(display = "There are no sections.")]
struct ErrNoSection;

/// Write to stdout.
// TODO move this into its own module ?
fn stdout_write(s: &mut Stdout, bytes: &[u8]) -> io::Result<()> {
    s.write_all(bytes)?;
    // For some reason some kinds of data might not be written to stdout without
    // flushing. Data that might fail to be written starts with `\0` (null).
    s.flush()
}


/// Byte used as delimiter between segments.
const DELIMITER: u8 = b'.';

/**
Steps:

* Get the section
* de-base64 it
* use first `HEADER_BYTES` for `Header`
* decrypt first section
* iterate over following sections, decrypting them
* while iterating, increment nonce before each decryption
*/
fn decryption_loop(passwd: &[u8]) -> Result<(), Error> {
    let mut stdout = io::stdout();
    let stdin = io::stdin();
    let mut sections = stdin.lock().split(DELIMITER);

    let mut header: Header;
    let key: Key;

    let mut bytes = Vec::new();

    {
        base64_decode(&sections.next().ok_or(ErrNoSection)??, &mut bytes)?;

        header = Header::from_slice(&bytes)?;
        key = get_key(passwd, header.salt());

        let bytes = &bytes[HEADER_BYTES..];

        let decrypted = decrypt(bytes, header.nonce(), &key)?;

        stdout_write(&mut stdout, &decrypted)?;
    } // free no longer needed memory

    while let Some(section) = sections.next() {
        let section = section?;
        // can be an "empty" section with only newlines/whitespaces
        // TODO perhaps instead of this properly sanitize input data
        if section.len() < 4 {
            return Ok(())
        }

        bytes.clear();
        header.increment_nonce();

        base64_decode(&section, &mut bytes)?;

        // there's an empty "section" at EOF
        if bytes.is_empty() { return Ok(()) }

        let decrypted = decrypt(&mut bytes, header.nonce(), &key)?;

        stdout_write(&mut stdout, &decrypted)?;
    }

    Ok(())
}

/**
Maximum size of data segment to be read from stdin.

Size chosen based on tests performed on AMD FX-8370 @ 4.0 GHz.

Throughput performance:

| Buffer size (KiB) | Encryption (MiB/s) | Decryption (MiB/s) |
|-------------------+--------------------+--------------------|
|              1024 |                274 |                193 |
|               512 |                341 |                220 |
|                64 |                340 |                247 |
|                32 |                350 |                241 |
|                 8 |                279 |                213 |

64 KiB segment was chosen based on the best decryption speed.
*/
const SEG_MEM_ALLOC: usize = 1024 * 64;

fn encryption_loop(pass: &[u8]) -> Result<(), Error> {
    let mut stdout = io::stdout();
    let stdin = io::stdin();

    let mut header = Header::new();
    let key = get_key(pass, header.salt());

    // use to store plaintext data read from stdin, clear afterwards
    let mut tmp = Vec::with_capacity(SEG_MEM_ALLOC + MACBYTES);

    // first encrypted segment differs a bit from the following ones,
    // since it contains the header
    {
        let mut buffer = Vec::with_capacity(SEG_MEM_ALLOC + HEADER_BYTES);
        buffer.extend_from_slice(&header.as_bytes());

        let mut handle = stdin.lock().take(SEG_MEM_ALLOC as u64);

        // EOF reached
        if handle.read_to_end(&mut tmp)? == 0 {
            return Ok(())
        }

        let encrypted = encrypt(&tmp, header.nonce(), &key);
        buffer.extend_from_slice(&encrypted);

        stdout_write(&mut stdout, base64_encode(&buffer).as_bytes())?;
        stdout_write(&mut stdout, &[DELIMITER])?;

        header.increment_nonce();
        tmp.clear();
    } // free no longer needed memory

    loop {
        let mut handle = stdin.lock().take(SEG_MEM_ALLOC as u64);

        // EOF reached
        if handle.read_to_end(&mut tmp)? == 0 {
            break;
        }

        let encrypted = encrypt(&tmp, header.nonce(), &key);

        stdout_write(&mut stdout, base64_encode(&encrypted).as_bytes())?;
        stdout_write(&mut stdout, &[DELIMITER])?;

        header.increment_nonce();
        tmp.clear();
    }

    Ok(())
}


/// Operation to run with.
#[derive(Clone, Debug)]
enum Operation {
    /// Encrypt with the password.
    Encrypt(Vec<u8>),
    /// Decrypt with the password.
    Decrypt(Vec<u8>),
    /// Print help message.
    Help,
    /// Print error, help message, and exit with `1` exit code. Call
    /// `Self::invalid_args()`.
    Invalid,
}

/// Name of env variable that can point to the keyfile.
const ENV_KEYFILE: &str = "EPC_KEYFILE";
/// Name of env variable that can contain password.
const ENV_PASSWD: &str = "EPC_PASSWD";


/*
- check for the presence of epaste environment variables
- EPC_PASSWD takes precedence over password
- EPC_KEYFILE takes precedence over EPC_PASSWD
- if there is more than one of password, EPC_PASSWD or EPC_KEYFILE
specified, print warning to stderr
- if EPC_KEYFILE points to invalid file error out, even if EPC_PASSWD
or password are available

 */

/// Function to read all bytes from supplied path.
// TODO move this into its own module ?
fn read_whole<P>(p: P) -> Result<Vec<u8>, Error>
where P: AsRef<std::path::Path>
{
    let mut file = File::open(p)?;
    let mut ret = Vec::new();
    file.read_to_end(&mut ret)?;
    Ok(ret)
}

impl Operation {
    /// Determine operation to perform.
    fn determine_op() -> Result<Self, Error> {
        let mut keyfile_bytes = None;
        if let Some(k) = env::var_os(ENV_KEYFILE) {
            keyfile_bytes = Some(read_whole(k)?);
        }

        let mut env_pass = None;
        if let Some(p) = env::var_os(ENV_PASSWD) {
            if keyfile_bytes.is_some() {
                eprintln!("Error: Both '{kf}' and '{ep}' are defined in \
                           the environment! Define only '{kf}' or '{ep}'.",
                          kf = ENV_KEYFILE,
                          ep = ENV_PASSWD);
                return Ok(Operation::Invalid)
            } else {
                env_pass = Some(p.as_bytes().to_vec());
            }
        }


        macro_rules! return_if_some {
            ($v:expr, $op:ident) => (
                if let Some(b) = $v {
                    return Ok(Operation::$op(b))
                }
            );
            ($op:ident) => (
                return_if_some!(keyfile_bytes, $op);
                return_if_some!(env_pass, $op);
            );
        }

        macro_rules! ret_invalid_if_too_much {
            () => (
                if keyfile_bytes.is_some() {
                    eprintln!("Error: Both password and '{kf}' supplied! \
                               Supply either password or '{kf}'.",
                              kf = ENV_KEYFILE);
                    return Ok(Operation::Invalid)
                }
                if env_pass.is_some() {
                    eprintln!("Error: Both password and '{ep}' supplied! \
                               Supply either password or '{ep}'.",
                              ep = ENV_PASSWD);
                    return Ok(Operation::Invalid)
                }
            )
        }

        match env::args().count() {
            1 => {
                return_if_some!(Encrypt);
                Ok(Operation::Invalid)
            },
            2 => {
                match &*env::args().nth(1).unwrap() {
                    "-h" | "--help" => Ok(Operation::Help),
                    "-d" => {
                        return_if_some!(Decrypt);
                        Ok(Operation::Invalid)
                    },
                    p => {
                        ret_invalid_if_too_much!();
                        Ok(Operation::Encrypt(Vec::from(p)))
                    },
                }
            },
            3 => {
                match &*env::args().nth(1).unwrap() {
                    "-d" => {
                        ret_invalid_if_too_much!();
                        let p = Vec::from(env::args().last().unwrap());
                        Ok(Operation::Decrypt(p))
                    },
                    _ => Ok(Operation::Invalid),
                }
            },
            _ => Ok(Operation::Invalid),
        }
    }

    /// Failed to parse args, print error msg, help, and exit(1).
    fn invalid_args() {
        help();
        process::exit(1);
    }

    /// Proceed with the operation.
    fn do_it(&self) {
        match *self {
            Operation::Encrypt(ref p) =>
                encryption_loop(p).map_err(|e| {
                    eprintln!("Error while encrypting: {}", e);
                    process::exit(1);
                }).unwrap(),
            Operation::Decrypt(ref p) =>
                decryption_loop(p).map_err(|e| {
                    eprintln!("Error while decrypting: {}", e);
                    process::exit(1);
                }).unwrap(),
            Operation::Help => help(),
            Operation::Invalid => Operation::invalid_args(),
        }
    }
}

/**
If crypto fails to initialize, exit with a non-zero exit code.

To be called first in `main()`.
*/
fn init_crypto_or_exit() {
    if sodiumoxide::init().is_err() {
        eprintln!("Error: failed to initialize crypto.");
        process::exit(1);
    }
}


fn main() {
    init_crypto_or_exit();
    Operation::determine_op().map_err(|e| {
        eprintln!("Error getting args: {}", e);
        process::exit(1);
    }).unwrap().do_it();
}
