/*
    Copyright © 2016-2019 Zetok Zalbavar <zexavexxe@gmail.com>

    This file is part of Epaste.

    Epaste is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Epaste is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epaste.  If not, see <http://www.gnu.org/licenses/>.
*/


//! Crypto stuff for Epaste.


use sodiumoxide::crypto::secretbox::*;
pub use sodiumoxide::crypto::secretbox::{seal as encrypt, Key, MACBYTES};
use sodiumoxide::crypto::pwhash::*;



/// Errors for crypto related stuff.
#[derive(Debug, Fail, Eq, PartialEq)]
pub enum ErrCrypto {
    #[fail(display = "Not enough bytes for crypto header")]
    // TODO rename
    FailCryptoHeader,
    #[fail(display = "Failed to decrypt section, invalid key or data")]
    FailDecrypt,
}

/**
Struct for storing data that is needed for deriving `Key`, and decrypting
encrypted payload.

Consists of:

* `Nonce`
* `Salt`
*/
pub struct Header {
    nonce: Nonce,
    salt: Salt,
}

/// Number of bytes of serialized [`Header`](./struct.Header.html).
pub const HEADER_BYTES: usize = NONCEBYTES + SALTBYTES;

impl Header {
    /// Create a new `ToDecBytes` with random `Nonce` and `Salt`.
    pub fn new() -> Self {
        Header {
            nonce: gen_nonce(),
            salt: gen_salt(),
        }
    }

    /// Return its `Nonce`.
    pub fn nonce(&self) -> &Nonce {
        &self.nonce
    }

    /// Return its `Salt`.
    pub fn salt(&self) -> &Salt {
        &self.salt
    }

    /**
    Decode bytes into the `Header`.

    Can only fail if there are not enough bytes (less than [`HEADER_BYTES`]).

    [`HEADER_BYTES`]: ./constant.HEADER_BYTES.html
    */
    pub fn from_slice(bytes: &[u8]) -> Result<Self, ErrCrypto> {
        if bytes.len() < HEADER_BYTES { return Err(ErrCrypto::FailCryptoHeader) }

        let nonce = Nonce::from_slice(&bytes[..NONCEBYTES])
            .ok_or(ErrCrypto::FailCryptoHeader)?;
        let salt = Salt::from_slice(&bytes[NONCEBYTES..(NONCEBYTES + SALTBYTES)])
            .ok_or(ErrCrypto::FailCryptoHeader)?;

        Ok(Header { nonce: nonce, salt: salt })
    }

    /// Increment `Nonce`. Increment before encrypting/decrypting each section.
    pub fn increment_nonce(&mut self) {
        self.nonce.increment_le_inplace();
    }


    /// Encode `Header` into bytes.
    pub fn as_bytes(&self) -> Vec<u8> {
        let mut result = Vec::with_capacity(HEADER_BYTES);
        result.extend_from_slice(&self.nonce.0);
        result.extend_from_slice(&self.salt.0);
        result
    }
}


/// Compute the secret Key.
pub fn get_key(passwd: &[u8], salt: &Salt) -> Key {
    let mut key = Key([0; KEYBYTES]);
    drop(derive_key(&mut key.0,
                    passwd,
                    salt,
                    OPSLIMIT_INTERACTIVE,
                    MEMLIMIT_INTERACTIVE)
            .expect("Failed to derive key."));
    assert!(key.0 != [0; KEYBYTES]);
    key
}


/**
Wrapper around `sodiumoxide`'s `secretbox::open()` that maps error to one that
provides a proper message.
 */
pub fn decrypt(c: &[u8], nonce: &Nonce, key: &Key) -> Result<Vec<u8>, ErrCrypto>
{
    open(c, nonce, key).map_err(|_| ErrCrypto::FailDecrypt)
}


#[cfg(test)]
mod test {
    use super::*;

    use quickcheck::quickcheck;
    use rand;

    use sodiumoxide::init;

    // get_key()

    quickcheck! {
        fn get_key_qc(passwd: Vec<u8>) -> bool {
            assert!(init().is_ok());
            get_key(&passwd, &gen_salt()) != Key([0; KEYBYTES])
        }
    }


    // decrypt()

    quickcheck! {
        fn decrypt_random(data: Vec<u8>) -> bool {
            assert!(init().is_ok());
            let nonce = &gen_nonce();
            let key = &gen_key();
            let c = seal(&data, nonce, key);

            let po = open(&c, nonce, key).unwrap();
            let pd = decrypt(&c, nonce, key).unwrap();
            po == pd && pd == data
        }
    }

    #[test]
    fn decrypt_random_fail() {
        fn f(data: Vec<u8>, invalid: Vec<u8>) {
            assert!(init().is_ok());
            let nonce = &gen_nonce();
            let key = &gen_key();
            let c = &seal(&data, nonce, key);

            let invalid_nonce = &gen_nonce();
            let invalid_key = &gen_key();

            macro_rules! wrong {
                ($($c:expr, $nonce:ident, $key:ident),+) => ($(
                    {
                        assert_eq!(decrypt($c, $nonce, $key).unwrap_err(),
                                   ErrCrypto::FailDecrypt);
                    }
                )+)
            }

            wrong!(
                // nonce
                c, invalid_nonce, key,
                // key
                c, nonce, invalid_key,
                // nonce & key
                c, invalid_nonce, invalid_key
            );
            // not enough bytes for MAC
            for n in 0..(MACBYTES-1) {
                let v = vec![rand::random(); n];
                wrong!(&v, nonce, key);
            }
            // invalid data
            if invalid.len() < MACBYTES {
                let mut v = Vec::with_capacity(MACBYTES + invalid.len());
                v.extend_from_slice(&[rand::random(); MACBYTES]);
                v.extend_from_slice(invalid.as_slice());
                wrong!(&v, nonce, key);
            } else {
                wrong!(&invalid, nonce, key);
            }
        }
        quickcheck(f as fn(Vec<u8>, Vec<u8>));
    }
}
