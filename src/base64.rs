/*

    Copyright © 2018-2019 Zetok Zalbavar <zexavexxe@gmail.com>

    This file is part of Epaste.

    Epaste is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Epaste is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epaste.  If not, see <http://www.gnu.org/licenses/>.
*/


//! Usability wrapper around `base64` crate.


extern crate base64;
use self::base64::*;


lazy_static! {
    /// Config for base64.
    static ref BASE64_CONFIG: Config = Config::new(
        CharacterSet::Standard,
        true    // pad
    );
}

/// Wrapper that simplifies encoding base64.
pub fn base64_encode<T>(input: &T) -> String
where T: ?Sized + AsRef<[u8]>
{
    //encode_config(input, *BASE64_CONFIG)
    let mut buf = String::with_capacity(input.as_ref().len() / 5 * 7);
    encode_config_buf(input, *BASE64_CONFIG, &mut buf);
    buf
}

/// Wrapper that simplifies decoding base64.
pub fn base64_decode<T>(input: &T, output: &mut Vec<u8>)
                        -> Result<(), DecodeError>
where T: ?Sized + AsRef<[u8]>
{
    decode_config_buf(input, *BASE64_CONFIG, output)?;
    Ok(())
}



#[cfg(test)]
mod test {
    use super::*;


    // base64_encode()

    quickcheck! {
        fn base64_encode_random(bytes: Vec<u8>) -> bool {
            bytes == decode_config(base64_encode(&bytes).as_bytes(),
                                   *BASE64_CONFIG).unwrap()
        }
    }

    // base64_decode()

    quickcheck! {
        fn base64_decode_random(bytes: Vec<u8>) -> bool {
            let mut out = Vec::with_capacity(bytes.len());
            base64_decode(encode_config(&bytes, *BASE64_CONFIG)
                          .as_bytes(), &mut out).unwrap();
            bytes == out
        }
    }
}
