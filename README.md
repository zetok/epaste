# Epaste [![build status](https://gitlab.com/zetok/epaste/badges/master/pipeline.svg)](https://gitlab.com/zetok/epaste/commits/master)

**Epaste** encrypts given data and turns encrypted bytes into `base64` text
which can be easily pasted/retrieved on e.g. pastebin website. Or any other
website that allows to post text on it.

User needs to supply a password or a keyfile that will be used to encrypt /
decrypt the data.

Decrypting can fail only if provided data is not valid.


## Usage

**When decrypting non-plaintext data take care to redirect Epaste output
to a file!**

### Using password from command line

To encrypt data:

```bash
epaste 'password' < 'file.txt'
```

To decrypt data into a file:

```bash
epaste -d 'password' < 'file.txt.epc' > 'file.txt'
```

Of course, you can easily encrypt some short text message without using file
redirection:

```bash
echo "Text to be encrypted" | epaste 'password'

# or even simpler
epaste 'password' <<<"Arguably, this is simpler."
```

### Using a keyfile

To encrypt:

```bash
EPC_KEYFILE="/path/to/a/keyfile" epaste < 'file.txt'
```

To decrypt into a file:

```bash
EPC_KEYFILE="/path/to/a/keyfile" epaste -d < 'encrypted_file.epc' > 'plaintext'
```

#### Suggested way of creating a keyfile

```bash
export KEYFILE=some_keyfile_name
touch $KEYFILE
# prevent anyone but you from reading/writing keyfile
chmod 600 $KEYFILE

# now that only you can access it, put in it random bytes
head -c 8192 /dev/urandom > $KEYFILE

# now that there are random bytes in it, make it read-only,
# so that hopefully it will not be accidentally modified
chmod 400 $KEYFILE
unset $KEYFILE
```

### Using password via environment variable

To encrypt:

```bash
EPC_PASSWD='password' epaste < 'file.txt'
```

To decrypt into a file:

```bash
EPC_PASSWD='password' epaste -d < 'encrypted.epc' > 'plaintext'
```

### Cleanup encoded data

If you've pasted your encrypted data on a website and retrieved it later
on, you might find that suddenly epaste can no longer decrypt it due to
the website messing with it and perhaps adding some invisible characters,
newlines or who knows what to it.

Although currently `epaste` doesn't try to sanitize the data, there's a
way to do it using other tools.

For example using `GNU AWK`:

```bash
awk -b 'BEGIN { FS=""; RS=""; } { gsub("[^a-zA-Z0-9=/+\.]", ""); print }' < data.epc > clean.epc
```

Although this does the job, performance is rather poor, with throughput at
about 2.5 MiB/s.

### More examples

Can be found on the wiki: https://gitlab.com/zetok/epaste/wikis/home

## Packages

* [Arch Linux](https://aur.archlinux.org/packages/epaste/)


## Dependencies
| **Name** | **Version** |
|----------|-------------|
| libsodium | >=1.0.4 |

## Building

You'll need [Rust] and [libsodium].

When you'll have deps, build debug version with

```bash
cargo build
```

## Install

Provided that you have [libsodium] installed, the easiest way is to use
[`Cargo`]:

```bash
cargo install epaste
```

Don't forget to add place where Cargo installs binaries to your `PATH` for
convenience, e.g.:

```bash
echo 'export PATH="$PATH:$HOME/.cargo/bin"' >> ~/.bashrc
source ~/.bashrc
```

## Updating

In case of newer version, updating is almost like installing:

```bash
cargo install --force epaste
```

## Goals

- [x] encrypt/decrypt message with given password
- [x] CLI interface
- [ ] manpage (to do with `ronn` or similar?)
  - installable system-wide (with `make`?)
- [ ] proper GUI interface
  - [ ] easy to distribute with CLI interface, preferably self-contained in a
        single file

## Support

If you like Epaste, feel free to help it by contributing, whether that would be
by writing code, suggesting improvements, or by donating.

Donate via Bitcoin: `1FSDbXVbUZSe34UqxJjfNMdAA9P8c6tNFQ`

## License

Licensed under GPLv3+. For details, see [COPYING](/COPYING).

[`Cargo`]: https://doc.rust-lang.org/cargo/
[libsodium]: https://github.com/jedisct1/libsodium
[Rust]: https://www.rust-lang.org/
